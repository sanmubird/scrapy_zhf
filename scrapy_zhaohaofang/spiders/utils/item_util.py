import re
import time

from w3lib.html import remove_tags


def clear_tag(value):
    if not value:
        return None
    else:
        # 移除标签
        content = remove_tags("".join(value))
        # 移除空格 换行
        return re.sub(r'[\t\r\n\s]', '', content)


# 先判断抓取的是否为空，如果不为空，再抓取第一个
def fetch_and_extract_first_by_css_rule(self, chinese, rule, response):
    fetch = clear_tag(response.css(rule).extract_first())
    if not fetch:
        self.logger.info('抓取到的 %s 内容为: %s ； 当前的url是：%s, 当前的页面引自于: %s ' % (
            chinese, fetch, response.url, bytes.decode(response.request.headers["Referer"])))
        return None
    else:
        return fetch


# 将多个 空格 压缩成一个
def str_compress_blank(origin_str):
    return re.sub('\s+', ' ', origin_str.strip())


def get_now_date():
    return time.strftime('%Y-%m-%d', time.localtime(time.time()))


def get_now_year():
    return time.strftime('%Y', time.localtime(time.time()))


# 组装item
# assemble_type 有两种类型 : zero_or_one , text_only ,  key_words 关键字 是对这些关键字 特殊处理逻辑的条件,
# 一般情况下 包含关键字 则会被处理为 放弃 或者 赋值 0
def extract_and_assemble_item(self, chinese, rule, response, target, item, assemble_type, key_words=None):
    extract_str = fetch_and_extract_first_by_css_rule(self, chinese, rule, response)
    if assemble_type == 'zero_or_one':
        if not extract_str:
            item[target] = 0
        else:
            if key_words and key_words in extract_str:
                item[target] = 0
            else:
                item[target] = 1
    elif assemble_type == 'text_only':
        if not extract_str:
            pass
        else:
            if key_words and key_words in extract_str:
                pass
            else:
                item[target] = extract_str
    else:
        self.logger.info("assemble_type 只能是[zero_or_one,text_only]这两种类型中的一个")

    return item
