# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class ScrapyZhaohaofangItem(scrapy.Item):
    # 照片id
    # 照片表
    pic_pid = scrapy.Field()
    # *必备
    pic_paths = scrapy.Field()
    # *必备
    pic_urls = scrapy.Field()

    # 小区 表
    qrt_id = scrapy.Field()
    # *必备
    qrt_name = scrapy.Field()
    # *必备
    qrt_addr = scrapy.Field()
    qrt_dsct = scrapy.Field()
    qrt_dnct = scrapy.Field()

    # 物业表
    prt_id = scrapy.Field()
    prt_name = scrapy.Field()

    # 品牌id
    bid = scrapy.Field()

    # 房屋表
    name = scrapy.Field()
    cbd = scrapy.Field()

    knd = scrapy.Field()
    aret = scrapy.Field()
    areu = scrapy.Field()

    ctmc = scrapy.Field()
    ctmb = scrapy.Field()
    ctmd = scrapy.Field()

    room = scrapy.Field()
    hal = scrapy.Field()
    tlt = scrapy.Field()
    ornt = scrapy.Field()
    flr = scrapy.Field()
    flrs = scrapy.Field()

    dcrt = scrapy.Field()
    noo = scrapy.Field()
    prct = scrapy.Field()
    prcu = scrapy.Field()

    kwd = scrapy.Field()
    dsc = scrapy.Field()
    cnt = scrapy.Field()
    pxf = scrapy.Field()

    cntr = scrapy.Field()
    cntrg = scrapy.Field()
    cntrt = scrapy.Field()
    cntrm = scrapy.Field()
    ptn = scrapy.Field()

    # 租房表相关信息
    mny = scrapy.Field()
    nny = scrapy.Field()
    pny = scrapy.Field()
    dpst = scrapy.Field()
    pay = scrapy.Field()
    rom = scrapy.Field()
    gdr = scrapy.Field()
    area = scrapy.Field()
    ctm = scrapy.Field()
    ttl = scrapy.Field()
    mbl = scrapy.Field()
    tel = scrapy.Field()


