import datetime
import logging

import pymysql
import scrapy
from twisted.enterprise import adbapi

from scrapy_zhaohaofang import settings
from scrapy_zhaohaofang.spiders.utils.item_util import get_now_year


class ZhfBaseSpider(scrapy.Spider):

    def __init__(self, begin_date=531, max_page_num=80, *args, **kwargs):
        super().__init__(self.name, **kwargs)
        begin_date = str(begin_date)
        if len(begin_date) < 3:
            raise NameError('爬取开始日期长度过短,当前值是: %s .' % begin_date)
        else:
            begin_date = str(begin_date)
            begin_date_str = "".join([get_now_year(), "-", begin_date[:-2], "-", begin_date[-2:]])
            self.begin_date = datetime.datetime.strptime(begin_date_str, '%Y-%m-%d')
            self.max_page_num = int(max_page_num)

    face_dict = {"东": 0, "南": 1, "西": 2, "北": 3, "东南": 4, "东北": 5, "西南": 6, "西北": 7}
    room_type_dict = {"整租": 0, "主卧": 1, "次卧": 2, "隔断": 3}
    gdr_type_dict = {"男女不限": 0, "限男生": 1, "限女生": 2}
    rent_pay_type_dict = {"一": 1, "二": 2, "三": 3, "四": 4, "五": 5, "六": 6,
                          "七": 7, "八": 8, "九": 9, "十": 10, "十一": 11, "十二": 12}
    brand_crm_dict = {
        "柠果": {
            "bid": "11",
            "cntr": "武钟磊",
            "cntrt": "15275157981",
            "cntrm": "18766139190",
            "prt_name": "柠果公寓物业",
        },
        "高新中合公寓": {
            "bid": "7",
            "cntr": "中合公寓",
            "cntrt": "15910088250",
            "cntrm": "0531-58058750",
            "prt_name": "中合公寓物业",
        },
        "中合公寓": {
            "bid": "7",
            "cntr": "中合公寓",
            "cntrt": "15910088250",
            "cntrm": "0531-58058750",
            "prt_name": "中合公寓物业",
        },
        "山东不期而遇": {
            "bid": "8",
            "cntr": "不期而遇",
            "cntrt": "",
            "cntrm": "0531-68816991",
            "prt_name": "不期而遇公寓物业",
        },
        # "柠果": {
        #     "cntr": "武钟磊",
        #     "cntrt": "15275157981",
        #     "cntrm": "18766139190",
        # },
        # "柠果": {
        #     "cntr": "武钟磊",
        #     "cntrt": "15275157981",
        #     "cntrm": "18766139190",
        # },
        # "柠果": {
        #     "cntr": "武钟磊",
        #     "cntrt": "15275157981",
        #     "cntrm": "18766139190",
        # },
        # "柠果": {
        #     "cntr": "武钟磊",
        #     "cntrt": "15275157981",
        #     "cntrm": "18766139190",
        # },
        # "柠果": {
        #     "cntr": "武钟磊",
        #     "cntrt": "15275157981",
        #     "cntrm": "18766139190",
        # },
    }

    def parse(self, response):
        self.logger.info("")


class ZhfBaseDBPipeline(object):
    def __init__(self, *args, **kwargs):
        self.dbpool_pro = adbapi.ConnectionPool(
            dbapiName='pymysql',
            host=settings.DB_CONF['host'],
            port=settings.DB_CONF['port'],
            db=settings.DB_CONF['db'],
            user=settings.DB_CONF['user'],
            passwd=settings.DB_CONF['passwd'],
            charset='utf8',
            cursorclass=pymysql.cursors.DictCursor,
            use_unicode=True)

    @staticmethod
    def handle_error(e):
        logging.error("sql error:%s", e)
